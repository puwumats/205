package library;

import library.entities.ICalendar;
import library.entities.helpers.*;
import org.junit.jupiter.api.Test;
import java.util.Date;
import static org.junit.jupiter.api.Assertions.*;

class FineCalculationIncorrectTestH0 {

    CalendarFileHelper cFH;
    ICalendar cal;
    Date currDate;
    long daysDif;
    long expDaysDif;

    @Test
    public void testOverdueTime() {
        cFH = new CalendarFileHelper();

        expDaysDif = 3;
        cal = cFH.loadCalendar();
        currDate = cal.getDate();
        cal.incrementDate(3);
        daysDif = cal.getDaysDifference(currDate);
        System.out.println("Current date:" + currDate);
        System.out.println("Forward Date:" + cal.getDate());

        assertEquals(expDaysDif, daysDif);
    }
}