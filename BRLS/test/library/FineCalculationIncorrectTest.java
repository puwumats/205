package library;

import library.entities.*;
import library.entities.helpers.*;
import org.junit.jupiter.api.Test;
import java.util.Date;
import static org.junit.jupiter.api.Assertions.assertEquals;

class FineCalculationIncorrectTest {

    //patron
    String ln = "Hatsune";
    String fn = "Miku";
    String em = "hm@h.com";
    long pNo = 42069;

    //book
    String tit = "The Communist Manifesto";
    String aut = "Karl Marx";
    String callNo = "c1";

    //objects
    LibraryFileHelper lFH;
    CalendarFileHelper cFH;
    ICalendar cal;
    ILibrary lib;
    IPatron pat;
    IBook book;
    ILoan loan;

    //fine related
    Date currDate;
    double actualFine;
    double desiredFine;

    @Test
    public void testIncorrectCalcOfFines() {

        //arrange
        desiredFine = 2;
        lFH = new LibraryFileHelper(new BookHelper(), new PatronHelper(), new LoanHelper());
        cFH = new CalendarFileHelper();

        //act
        cal = cFH.loadCalendar();
        lib = lFH.loadLibrary();
        pat = lib.addPatron(ln, fn, em, pNo);
        book = lib.addBook(aut, tit, callNo);
        loan = lib.issueLoan(book, pat);
        lib.commitLoan(loan);
        cal.incrementDate(4);
        currDate = cal.getDate();
        loan.updateOverDueStatus(currDate);
        pat.incurFine(lib.calculateOverDueFine(loan));
        loan.discharge(false);
        actualFine = pat.getFinesPayable();

        //assert
        assertEquals(desiredFine, actualFine);
    }
}