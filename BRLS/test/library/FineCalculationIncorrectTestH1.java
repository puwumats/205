package library;

import library.entities.ICalendar;
import library.entities.helpers.CalendarFileHelper;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FineCalculationIncorrectTestH1 {

    CalendarFileHelper cFH;
    ICalendar cal;
    long currDate;
    long forwardDate;
    long diffMilliseconds;
    long diffDays;
    long millisPerDay;

    @Test
    public void testGetDaysDifference() {
        millisPerDay = 86400000;
        cFH = new CalendarFileHelper();
        cal = cFH.loadCalendar();

        currDate = cal.getDate().getTime();
        cal.incrementDate(3);

        forwardDate = cal.getDate().getTime();

        diffMilliseconds = currDate - forwardDate;
        diffDays = diffMilliseconds / millisPerDay;
        System.out.println("Days:" + diffDays);

        assertEquals(-3, diffDays);
    }
}